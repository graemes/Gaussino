# Generation
Configuration related to the generation phase.


```{eval-rst}
.. currentmodule:: Gaussino.Generation

.. autoclass:: GenPhase
   :show-inheritance:
   :members: setOtherProp, setOtherProps, configure_generation, configure_pgun, configure_phase, configure_genonly, event_type
   :undoc-members:
   :special-members: __apply_configuration__
```
